﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class VisiteSortMedecin : IComparer<Visite>
    {
        public int Compare(Visite x, Visite y)
        {
            return x.NomMedecin.CompareTo(y.NomMedecin);
        }
    }
}
