﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    public class Ligne
    {
        private Medicament medicament;
        private int qte;
        private int prixLigne;

        public Ligne(Medicament m, int qte)
        {
            this.medicament = m;
            this.qte = qte;
            prixLigne = m.Prix * this.qte;
        }
        public Medicament Medicament
        {
            get { return medicament; }
        }
        public int PrixLigne
        {
            get { return prixLigne; }
        }
        public int Qte
        {
            get { return qte; }
        }
        public override string ToString()
        {
            return "Quantité : " + this.qte + " - " + medicament.ToString() + " prixLigne : " + prixLigne + " EURO(S)";
        }
    }
}
