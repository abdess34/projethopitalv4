﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class DaoVisite
    {   
        private string connectionString = @"Data Source=LAPTOP-3T9H10D3\SQLEXPRESS;Initial Catalog=hn-hopital;Integrated Security=True";

        public List<Visite> SelectAllVisites()
        {
            //"SELECT *FROM visites"
            //string connectionString = @"Data Source=LAPTOP-PP2V8KLN\SQLEXPRESS01;Initial Catalog=hn-hopital;Integrated Security=True";
            string sql = "SELECT * FROM visitesV2";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<Visite> listeVisites = new List<Visite>();
            while (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                int arg1b = int.Parse(arg1);
                string arg2 = "";
                arg2 += reader.GetValue(1);
                int arg2b = int.Parse(arg2);
                string arg3 = "";
                arg3 += reader.GetValue(2);
                string arg4 = "";
                arg4 += reader.GetValue(3);
                string arg5 = "";
                arg5 += reader.GetValue(4);
                int arg5b = int.Parse(arg5);

                Visite v = new Visite(arg1b, arg2b, arg3, arg4, arg5b);
                listeVisites.Add(v);
            }

            connection1.Close();

            return listeVisites;
        }

        public Visite SelectVisiteByPatientDate(int id, DateTime date)
        {
            string sql = "select idPatient,ordo,tarif from visitesV2 where idPatient=@id and date=@date";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            command.CommandText = sql;

            command.Parameters.Add("id", SqlDbType.Int).Value = id;
            command.Parameters.Add("date", SqlDbType.DateTime).Value = date;
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();
            Visite res = null;
            while (reader.Read())
            {
                //Console.WriteLine(reader.GetInt32(1));
                res = new Visite(0,reader.GetInt32(0),null,null,0,reader.GetInt32(2), reader.IsDBNull(1)?"":reader.GetString(1));
            }
            
                return res;
        }
        public List<String> SelectNbVisiteSalleMedecin()
        {
            string sql = "select numSalle,medecin,COUNT(*) from visitesV2 GROUP BY numSalle , medecin order by numSalle , medecin asc";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();
           
            List<String> res = new List<string>();
            while (reader.Read())
            {
                //Console.WriteLine(reader.GetInt32(1));
                //res = new Visite(0, reader.GetInt32(0), null, null, 0, reader.GetInt32(2), reader.IsDBNull(1) ? "" : reader.GetString(1));


                res.Add("numSalle : " + reader.GetInt32(0) + " medecin : " + reader.GetString(1) + " nbVisites :" + reader.GetInt32(2));
            }

            return res;
        }
        public List<Visite> SelectByMedecin(String nom)
        {
            //select * from Visites where visites.medecin
            //string connectionString = @"Data Source=LAPTOP-PP2V8KLN\SQLEXPRESS01;Initial Catalog=hn-hopital;Integrated Security=True";
            string sql = "select * from VisitesV2 where VisitesV2.medecin='" + nom + "'";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<Visite> listeVisites = new List<Visite>();
            while (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                int arg1b = int.Parse(arg1);
                string arg2 = "";
                arg2 += reader.GetValue(1);
                int arg2b = int.Parse(arg2);
                string arg3 = "";
                arg3 += reader.GetValue(2);
                string arg4 = "";
                arg4 += reader.GetValue(3);
                string arg5 = "";
                arg5 += reader.GetValue(4);
                int arg5b = int.Parse(arg5);

                Visite v = new Visite(arg1b, arg2b, arg3, arg4, arg5b);
                listeVisites.Add(v);
            }

            connection1.Close();

            return listeVisites;
        }
        public List<Visite> SelectByPatient(int id)
        {
            //select * from Visites where visites.medecin
            string sql = "select * from VisitesV2 where VisitesV2.idpatient='" + id + "'";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<Visite> listeVisites = new List<Visite>();
            while (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                int arg1b = int.Parse(arg1);
                string arg2 = "";
                arg2 += reader.GetValue(1);
                string arg3 = "";
                arg3 += reader.GetValue(2);
                int arg2b = int.Parse(arg2);
                string arg4 = "";
                arg4 += reader.GetValue(3);
                string arg5 = "";
                arg5 += reader.GetValue(4);
                int arg5b = int.Parse(arg5);
                string arg6 = "";
                arg6 += reader.GetValue(5);
                int arg6b = int.Parse(arg6);
                Visite v = new Visite(arg1b, arg2b, arg3, arg4, arg5b, arg6b);
                listeVisites.Add(v);
            }

            connection1.Close();

            return listeVisites;
        }
        public string SelectAttenteByIdDate(int idpatient, DateTime date)
        {
            string sql = "select tempsAttente from visitesV2 where idPatient=@id and date=@date";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            command.CommandText = sql;

            command.Parameters.Add("id", SqlDbType.Int).Value = idpatient;
            command.Parameters.Add("date", SqlDbType.DateTime).Value = date;
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();
            string res= "";
            while (reader.Read())
            {
                //Console.WriteLine(reader.GetInt32(1));
                res = reader.GetString(0);
            }

            return res;
        }
        public bool insertVisite(Visite v)
        {
            //string connectionString = @"Data Source=LAPTOP-PP2V8KLN\SQLEXPRESS01;Initial Catalog=hn-hopital;Integrated Security=True";
            SqlConnection connexion1 = new SqlConnection(connectionString);
            connexion1.Open();
            string sql = "insert into visitesV2 values(@idpatient,@date,@medecin,@numSalle,@tarif,@ordo,@temps)";
            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = connection1.CreateCommand();
            command.CommandText = sql;


            command.Parameters.Add("idpatient", SqlDbType.Int).Value = v.IdPatient;
            command.Parameters.Add("date", SqlDbType.DateTime).Value = v.Date;
            command.Parameters.Add("medecin", SqlDbType.NVarChar).Value = v.NomMedecin;
            command.Parameters.Add("numSalle", SqlDbType.Int).Value = v.NumSalle;
            command.Parameters.Add("tarif", SqlDbType.Int).Value = v.Tarif;
            command.Parameters.Add("ordo", SqlDbType.NVarChar).Value = v.Ordo;
            command.Parameters.Add("temps", SqlDbType.Time).Value = (v.Attente);
            connection1.Open();
            int count = command.ExecuteNonQuery();
            connection1.Close();
            return count > 0 ? true : false;
        }

        public bool Update(Visite v)
        {
            SqlConnection connexion1 = new SqlConnection(connectionString);
            connexion1.Open();
            string sql = "update  visitesV2 set idpatient =@idpatient,date =@date,medecin=@medecin,numSalle=@numSalle,tarif=@tarif,ordo=@ordo where id = @id";
            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = connection1.CreateCommand();
            command.CommandText = sql;

            command.Parameters.Add("id", SqlDbType.Int).Value = v.Id;
            command.Parameters.Add("idpatient", SqlDbType.Int).Value = v.IdPatient;
            command.Parameters.Add("date", SqlDbType.DateTime).Value = DateTime.Parse(v.Date);
            command.Parameters.Add("medecin", SqlDbType.NVarChar).Value = v.NomMedecin;
            command.Parameters.Add("numSalle", SqlDbType.Int).Value = v.NumSalle;
            command.Parameters.Add("tarif", SqlDbType.Int).Value = v.Tarif;
            command.Parameters.Add("ordo", SqlDbType.NVarChar).Value = v.Ordo;
            connection1.Open();
            int count = command.ExecuteNonQuery();
            connection1.Close();
            return count > 0 ? true : false;
        }
        //Admin.3.calculer le prix moyen des visites  pour un medecin depuis le debut a aujourdhui
        // SELECT AVG(tarif) FROM visites where medecin = 'Lefebvre'

        public double GetTarifMoyByMedecin(string nom)
        {
            string sql = " SELECT AVG(tarif) FROM visitesV2 where medecin = '" + nom + "'";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            double tarifMoy = 0;
            if (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                tarifMoy = double.Parse(arg1);
            }

            connection1.Close();

            return tarifMoy;
        }
        //4.calculer le prix moyen des visites  pour un medecin entre une date min et max
        //SELECT AVG(tarif) FROM visites where medecin='Lefebvre' AND date BETWEEN '01/01/2023 12:23:00.000' AND '19/08/2023 05:32:00.000'

        public double GetTarifMoyByMedecinAndDate(string nom, DateTime dateMin, DateTime dateMax)
        {
            string sql = " SELECT AVG(tarif) FROM visitesV2 where medecin = '" + nom + "' AND DATE BETWEEN '" + dateMin + "' AND '" + dateMax + "'";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            double tarifMoy = 0;
            if (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                tarifMoy = double.Parse(arg1);
            }

            connection1.Close();

            return tarifMoy;
        }
        public List<Visite> selectAllVisitesByPatient(int idPatient)
        {
            //SELECT * FROM visites WHERE idpatient = 1 ORDER BY date ASC;
            //select * from Visites where visites.medecin
            string sql = "select * from VisitesV2 where VisitesV2.idpatient='" + idPatient + "' ORDER BY date ASC";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<Visite> listeVisites = new List<Visite>();
            while (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                int arg1b = int.Parse(arg1);
                string arg2 = "";
                arg2 += reader.GetValue(1);
                string arg3 = "";
                arg3 += reader.GetValue(2);
                int arg2b = int.Parse(arg2);
                string arg4 = "";
                arg4 += reader.GetValue(3);
                string arg5 = "";
                arg5 += reader.GetValue(4);
                int arg5b = int.Parse(arg5);
                string arg6 = "";
                arg6 += reader.GetValue(5);
                int arg6b = int.Parse(arg6);
                Visite v = new Visite(arg1b, arg2b, arg3, arg4, arg5b, arg6b);
                listeVisites.Add(v);
            }

            connection1.Close();

            return listeVisites;

        }
        //3.afficher le nombre de visites pour un patient donné depuis le debut de son activité
        //Select Count(*) FROM visites where idpatient = 1 AND medecin='Lefebvre';
        public int GetNbVisitesByPatient(int idPatient)
        {
            string sql = "Select Count(*) FROM visitesv2 where visitesv2.idPatient=" + idPatient ;

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            int nbVisites = 0;
            if (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                nbVisites = int.Parse(arg1);
            }

            connection1.Close();

            return nbVisites;
        }

        //4.afficher le nombre de visites pour un patient donné entre une date a et une date b
        //SELECT Count(*) FROM visites where idpatient=1 AND DATE BETWEEN '01/01/2023 12:23:00.000' AND '19/08/2023 05:32:00.000'
        public int GetNbVisitesByPatientAndDate(int idPatient, DateTime dateMin, DateTime dateMax)
        {
            string sql = "Select Count(*) FROM visitesv2 where visitesv2.idPatient=" + idPatient + " AND DATE BETWEEN '" + dateMin + "' AND '" + dateMax + "'";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            int nbVisites = 0;
            if (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                nbVisites = int.Parse(arg1);
            }

            connection1.Close();

            return nbVisites;
        }

    }
}
