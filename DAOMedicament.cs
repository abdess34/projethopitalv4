﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    public class DAOMedicament
    {
        private string connectionString = @"Data Source=LAPTOP-3T9H10D3\SQLEXPRESS;Initial Catalog=hn-hopital;Integrated Security=True";

        public List<Medicament> SelectAllMedicaments()
        {
            //"SELECT *FROM visites"
            string sql = "SELECT * FROM medicaments";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<Medicament> listeMedicaments = new List<Medicament>();
            while (reader.Read())
            {

                Medicament v = new Medicament(reader.GetInt32(0),
                    reader.GetValue(1) != null ? reader.GetString(1) : "",
                     reader.GetValue(2) != null ? reader.GetInt32(2) : 0,
                     reader.GetValue(3) != null ? reader.GetInt32(3) : 0);
                listeMedicaments.Add(v);
            }

            connection1.Close();

            return listeMedicaments;
        }
        public bool Update(Medicament m)
        {
            SqlConnection connexion1 = new SqlConnection(connectionString);
            connexion1.Open();
            string sql = "update  medicaments set nom =@nom,prix =@prix,quantite=@qte where id = @id";
            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = connection1.CreateCommand();
            command.CommandText = sql;

            command.Parameters.Add("id", SqlDbType.Int).Value = m.Id;
            command.Parameters.Add("nom", SqlDbType.NVarChar).Value = m.Nom;
            command.Parameters.Add("prix", SqlDbType.Int).Value = m.Prix;
            command.Parameters.Add("qte", SqlDbType.Int).Value = m.Quantite;

            connection1.Open();
            int count = command.ExecuteNonQuery();
            connection1.Close();
            return count > 0 ? true : false;
        }
        public Medicament SelectById(int idMedicament)
        {
            ///string connectionString = @"Data Source=LAPTOP-PP2V8KLN\SQLEXPRESS01;Initial Catalog=hn-hopital;Integrated Security=True";
            string sql = "select  * from medicaments where id=" + idMedicament;

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            Medicament m = null;
            if (reader.Read())
            {
                m = new Medicament(reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2), 
                    reader.GetInt32(3)
                    );

            }

            connection1.Close();

            return m;
        }
    }
}
