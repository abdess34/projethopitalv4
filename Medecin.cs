﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    [Serializable]
    class Medecin:Authentification
    {
        private int seuilMax=10;
        private Salle salle;
        private List<Visite> listeVisites;
       


        public Medecin(string login, string password, string nom, int metier, int seuilMax, Salle salle ):base(login, password, nom, metier)
        {
            this.seuilMax = seuilMax;
            this.nom = nom;
            this.salle = new Salle(metier,true);
            this.listeVisites = new List<Visite>();
        }

        public Medecin(string login, string password, string nom, int metier) : base(login, password, nom, metier)
        {
            this.listeVisites = new List<Visite>();
        }


        public int SeuilMax
        {
            get
            {
                return seuilMax;
            }
        }

        public Salle Salle {
            get {
                return salle;
            }
            set { salle = value;}

        }

        public List<Visite> ListeVisites {
            get {
                return listeVisites;
            }
        }

        public void RendreDispo()
        {
            this.salle.Disponible= true;
        }

        public void Attache(Visite v)
        {
            listeVisites.Add(v);
            if (listeVisites.Count() == seuilMax)
            {
                //si c'est le cas on déclenche ajouterVisites(listeVisites)
                Console.WriteLine("----------------------------------------");

                Console.WriteLine("Seuil des visites atteind, Persistance des visites");
                Console.WriteLine("----------------------------------------");

                AjouterVisites();

                //et on vide listeVisites
                listeVisites = new List<Visite>();
            }
        }
        //public List<Patient> AfficherFile()
        //{
        //    string connectionString = @"Data Source=LAPTOP-PP2V8KLN\SQLEXPRESS01;Initial Catalog=hn-hopital;Integrated Security=True";
        //    string sql = "select * from Patients JOIN Visites ON patients.id=visites.idPatient";

        //    SqlConnection connection1 = new SqlConnection(connectionString);
        //    SqlCommand command = new SqlCommand(sql, connection1);
        //    connection1.Open();
        //    SqlDataReader reader = command.ExecuteReader();

        //    List<Patient> listePatients = new List<Patient>();
        //    while (reader.Read())
        //    {
        //        string arg1 = "";
        //        arg1 += reader.GetValue(0);
        //        int arg1b = int.Parse(arg1);
        //        string arg2 = "";
        //        arg2 += reader.GetValue(1);
        //        string arg3 = "";
        //        arg3 += reader.GetValue(2);
        //        string arg4 = "";
        //        arg4 += reader.GetValue(3);
        //        int arg4b = int.Parse(arg4);
        //        string arg5 = "";
        //        arg5 += reader.GetValue(4);
        //        string arg6 = "";
        //        arg6 += reader.GetValue(5);

        //        string arg7 = "";
        //          arg7+=  reader.GetValue(6);                              

        //        Patient p = new Patient(arg1b, arg2, arg3, arg4b, arg5,arg6);
        //        listePatients.Add(p);
        //    }

        //    connection1.Close();

        //    return listePatients;
        //}
            
        public void notifyNext()
        {
            /*Le médecin qui va devoir libérer le patient actuel, prévenir un autre patient et rendre la salle disponible.
            De son côté via le notify() le patient deviendra le patient actuel la classe passera de disponible à occupée et une instance de visite
            sera ajouter au médecin, il faudra vérifier si la liste des visites a une longueur correspondant au seuil max si c'est le cas
            ajouterListes(listeVisites) devra être déclencher*/

            //Une fois une consultation terminée, le médecin libère la salle et avertit le patient en haut de la liste qu'elle est disponible
            //Libérer le patient actuel
            Hopital.Instance.FileDattente.Dequeue();
            salle.PatientActuel=null;
            
            //Rendre la salle disponible: la salle n°n correspond au médecin n°n.
            RendreDispo();

            //Récupérer le prochain patient 
            Patient prochainPatient = Hopital.Instance.ProchainPatient();
            //Prévenir le prochain patient 
            

            if (prochainPatient != null)
            {

                //La salle passe de disponible à occupée
                salle.Disponible = false;
                //Le patient actuel est affecté
                salle.PatientActuel = prochainPatient;
                //notifier le patient
                prochainPatient.Medecin = this;
                prochainPatient.notify();
                //On le retire de la liste avec Hopital.Instance.RetirerPatient();
                //Une visite est créée
                Console.WriteLine("PatientActuel: " + Hopital.Instance.ProchainPatient());

                Console.WriteLine("Date actuelle: " + DateTime.Now.ToString());
                Console.WriteLine("Nom: " + nom);
                Console.WriteLine("Numéro de salle: " + salle.NumSalle);
                //Visite visite = new Visite(salle.PatientActuel.Id, DateTime.Now.ToString(), nom, salle.NumSalle);
                //listeVisites.Add(visite);
                //On vérifie si la longueur de la listeVisites de médecin à atteint le seuilMax 
                Console.WriteLine("Il y a " + listeVisites.Count() + " visites dans la liste");
                if (listeVisites.Count() == seuilMax)
                {
                    //si c'est le cas on déclenche ajouterVisites(listeVisites) 
                    Console.WriteLine("entrée");
                    AjouterVisites();
                    Console.WriteLine("Hello");
                    //et on vide listeVisites
                    listeVisites = new List<Visite>();
                    
                }
            }
            else
            {
                Console.WriteLine("Il n'y a plus de patient dans la file d'attente");
            }

        }

        public Patient notifyNextV2()
        {
            //rend la salle dispo
            RendreDispo();
            //Récupérer le prochain patient 

            Patient prochainPatient = Hopital.Instance.ProchainPatient();

            if (prochainPatient != null)
            {
                //Le patient actuel est affecté
                salle.PatientActuel = prochainPatient;
                //notifier le patient
                prochainPatient.Medecin = this;
                prochainPatient.notify();

                //Console.WriteLine("PatientActuel: " + prochainPatient);

                //La salle passe de disponible à occupée
                salle.Disponible = false;
                //Console.WriteLine("Il y a " + listeVisites.Count() + " visites dans la liste");


            }

            return prochainPatient;
        }
        public void AjouterVisites()
        {
            DaoVisite daoVisite = new DaoVisite();
            foreach (Visite v in listeVisites)
            {
              
                daoVisite.insertVisite(v);
            }
                

        }

        public Visite GetLastVisiste()
        {

            return listeVisites.Count != 0? listeVisites[listeVisites.Count - 1] : null;
        }
        public override string ToString()
        {
            return "Médecin: Nom: "+nom+" Seuil: " + seuilMax;
        }
    }
}
