﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class Hopital
    {
        private static Hopital instance = null;
        private Secretaire secretaire;
        private Queue<Patient> fileDattente;
        private List<Medecin> listeMedecins;
        private List<Medicament> listeMedicaments;
        private Hopital()
        {
            fileDattente = new Queue<Patient>();
            DaoAuthentification auth = new DaoAuthentification();
            listeMedecins = auth.SelectAllMedecin();
            foreach (Medecin m in listeMedecins)
            {
                m.Salle = new Salle(m.Metier,false);
            }
            secretaire = auth.SelectSecretaire();
            listeMedicaments = new DAOMedicament().SelectAllMedicaments();
        }
        public static Hopital Instance
        {
            get
            {
                if (instance == null)
                    instance = new Hopital();
                return instance;
            }
        }

        public Secretaire Secretaire
        {
            get { return secretaire; }
        }
        public List<Medecin> ListeMedecins
        {
            get { return listeMedecins; }
        }
        public List<Medicament> ListeMedicaments
        {
            get { return listeMedicaments; }
        }
        public Queue<Patient> FileDattente
        {
            get { return fileDattente; }
            
        }

        /// <summary>
        /// Ajoute un patient dans la file d'attente nommé fileDattente
        /// </summary>
        /// <param name="p"> Patient </param>
        public bool AjouterPatient(Patient p)
        {
            bool doublons = false;
            foreach (Patient b in fileDattente)
            {
                if (b.Id == p.Id)
                {
                    doublons = true;
                    break;
                }
            }
            if(!doublons) fileDattente.Enqueue(p);
            return doublons;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns> retrourne le prochain patient de la file (sans le retirer)</returns>
        public string ProchainPatientDansFile()
        {
            return fileDattente.Count != 0 ? fileDattente.Peek().ToString() : "File d'attente vide";
        }
        public Patient ProchainPatient()
        {
            
            return fileDattente.Count != 0 ? fileDattente.Peek():null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>retourne la file d’attente </returns>
        public override string ToString()
        {
            string res = "file d'attente:";
            foreach (Patient patient in fileDattente)
            {
                res += "\n" + patient.ToString();
            }
            return res;
        }

        public void printSecretaire()
        {
            Console.WriteLine(secretaire);
        }

        public void AffecterFileAttente(Queue<Patient> file)
        {
            foreach(Patient a in file)
            {
                AjouterPatient(a);
            }
        }
    }
}
