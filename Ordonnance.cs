﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    public class Ordonnance
    {
        private List<Ligne> mesLignes;
        private int idPatient;
        private string nom,prenom;
        private int prixT;
        public List<Ligne> MesLignes
        {
            get { return mesLignes; }
        }
        public Ordonnance(int idPatient)
        {
            this.mesLignes = new List<Ligne>();
            this.idPatient = idPatient;
        }
        public Ordonnance(string nom,string prenom)
        {
            this.mesLignes = new List<Ligne>();
            this.nom = nom; this.prenom = prenom;
            
        }
        public string Nom
        {
            get { return nom; }
        }
        public string Prenom
        {
            get { return prenom; }
        }
        public int IdPatient
        {
            get { return idPatient; }
        }
        public int PrixT
        {
            get { return prixT; }
            
        }
        public void AjouterLigne(Ligne l)
        {
            this.mesLignes.Add(l);
            prixT += l.PrixLigne;
        }
        public override string ToString()
        {
            string res = "Ordonnance Patient : " + this.idPatient +(this.nom ==null?"":this.nom)+ " "+(this.prenom == null ? "" : this.prenom) + "\n";
            foreach (Ligne l in mesLignes)
            {
                res += l.ToString() + "\n";
            }
            res += "TOTAL TTC: " + prixT;
            return res;
        }
    }
}
