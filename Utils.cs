﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    public class Utils
    {
        public static void AfficheEntete()
        {
            Console.WriteLine("********************************************************************");
            Console.WriteLine("**************** Bienvenue au service Hospitalier ******************");
            Console.WriteLine("********************************************************************");
        }
        public static void AfficheChoixInterface()
        {
            Console.WriteLine("[1] -> Interface Patient");
            Console.WriteLine("[2] -> Interface Personnels de l'hopital");
            Console.WriteLine("----------------------------------------");
        }
        public static void AfficheInterfaceSecretaire()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("****  OPERATIONS DE LA SECRETAIRE  ****");
            Console.WriteLine("[1] -> Ajouter un patient a la file d’attente ");
            Console.WriteLine("[2] -> Afficher la file d’attente");
            Console.WriteLine("[3] -> Afficher le prochain patient de la file");
            Console.WriteLine("[4] -> Modifier/completer la fiche d'un patient");
            Console.WriteLine("[5] -> Afficher historique des visite d'un patient");
            Console.WriteLine("[6] -> Retour au menu principal");
            Console.WriteLine("----------------------------------------");
        }
        public static void AfficheInterfaceMedecin()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("****      OPERATIONS DU MEDECIN     ****");
            Console.WriteLine("[1] -> Rendre la salle disponible ");
            Console.WriteLine("[2] -> Afficher la file d’attente");
            Console.WriteLine("[3] -> Afficher les visites");
            Console.WriteLine("[4] -> Sauvergarder les visites en base");
            Console.WriteLine("[5] -> Retour au menu principal");
            Console.WriteLine("----------------------------------------");
        }
        public static int RecuperChoixUtilisateur()
        {
            Console.WriteLine("Entrer votre choix : ");
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}
