﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class Salle
    {
        private Boolean disponible;
        private int numero;
        private Patient patientActuel;

        public Salle( int numero, bool disponible,Patient patientActuel)
        {
            this.disponible = disponible;
            this.numero = numero;
            this.patientActuel = patientActuel;
        }

        public Salle(int numero, bool disponible)
        {
            this.disponible = disponible;
            this.numero = numero;
            this.patientActuel = null;
        }


        public Boolean Disponible
        {
            set
            {
                disponible = value;
            }
        }

        public Patient PatientActuel {
            get
            {
                return patientActuel;
            }
            set
            {
                patientActuel=value;
            }
        }

        public int NumSalle {
            get
            {
                return numero;
            }
        }

        public override string ToString()
        {
            return "Salle n°"+numero+" Disponibilité: "+disponible+" patient Actuel: "+ patientActuel != null ?patientActuel.ToString():"aucun patient";
        }
    }
}
