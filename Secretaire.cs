﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;

namespace projetHopital_HN
{
    class Secretaire:Authentification
    {
        private const string path = "historique.text";
        private const string pathSerialisable = "mesPatient.bin";
        public Secretaire(string login, string password, string nom, int met) : base(login, password, nom, met)
        {

        }

        public bool AjouterPatientFileAttente(Patient p)
        {
            DateTime d = DateTime.Now;
            p.Debut = d;
            return Hopital.Instance.AjouterPatient(p);
            
        }
        public override string ToString()
        {
            return "SECRETAIRE: " + base.ToString();
        }
        public void SauvergarderDansFichierPatient(Patient p)
        {
            DateTime now = DateTime.Now;
            string formattedDate = now.ToString("dd/MM/yyyy HH'h'mm");

            if (!File.Exists(path))
            {
                using (StreamWriter sw = File.CreateText(path))
                {

                    sw.WriteLine(p.Id + " " + formattedDate);

                    sw.Close();
                }
            }
            else
            {
                StreamWriter sw = File.AppendText(path);
                sw.WriteLine(p.Id + " " + formattedDate);

                sw.Close();
            }
        }

        public void SauvergarderListAttente()
        {
            FileStream fs = File.Create(pathSerialisable);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, Hopital.Instance.FileDattente);
            fs.Close();
        }

        public void ChargerFileAttente()
        {
            if (File.Exists(pathSerialisable))
            {
                FileStream fs = null;
                try
                {
                    fs = File.OpenRead(pathSerialisable);
                    BinaryFormatter bf = new BinaryFormatter();
                    Hopital.Instance.AffecterFileAttente( (Queue < Patient >)  bf.Deserialize(fs));

                }catch(Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                        File.Delete(pathSerialisable);
                    }
                }
            }
        }
    }
}
