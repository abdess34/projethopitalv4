﻿using System;
namespace projetHopital_HN
{
    [Serializable]
    public abstract class Authentification
    {
        protected string login, password, nom;
        protected int metier = -1;

        public Authentification(string login, string password, string nom, int metier)
        {
            this.login = login;
            this.password = password;
            this.nom = nom;
            this.metier = metier;
        }
        public string Login
        {
            get { return login; }
        }
        public string Password
        {
            get { return password; }
        }
        public string Nom
        {
            get { return nom; }
        }
        public int Metier
        {
            get { return metier; }
        }
        public override string ToString()
        {
            return "LOGIN : " + login + " PASSWORD : " + password + " NOM : " + nom + " METIER : " + metier;
        }
    }
}