﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    public  class Tests
    {
        static void mesTestMedecin()
        {
            Tests.TestSelectByMedecin();//Validé
            Tests.TestSelectAllVisites();//Validé
            Tests.TestSelectById();// Validé
            Tests.TestSelectAllPatients();//Validé
            Tests.TestDispo();//  Validé
            Tests.TestNotifyNext();//Validé
            Tests.TestAjouterVisites();//Validé
            Tests.TestAfficherFile();//Validé
            Tests.TestToStringMedecin();//Validé
            Tests.TestToStringPatient();//Validé 
            Tests.TestToStringVisite();//Validé
            Tests.TestToStringSalle();//Validé 
        }
        public static void TestSelectByMedecin()
        {
            DaoVisite daoVisite = new DaoVisite();
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            //On récupère la liste des visites de ce médecin
            List<Visite> ListeVisites = daoVisite.SelectByMedecin("Lefebvre");

            //On affiche la liste des visites
            foreach (Visite v in ListeVisites)
                Console.WriteLine(v.ToString());
        }

        public static void TestSelectAllVisites()
        {
            DaoVisite daoVisite = new DaoVisite();
            //On récupère la liste de toutes les visites en base
            List<Visite> ListeVisites = daoVisite.SelectAllVisites();

            //On affiche la liste des visites
            foreach (Visite v in ListeVisites)
                Console.WriteLine(v.ToString());

        }

        public static void TestSelectAllPatients()
        {

            DaoPatient daoPatient = new DaoPatient();
            //On récupère la liste de tous les patient en base
            List<Patient> ListePatients = daoPatient.SelectAllPatients();

            //On affiche la liste des patient
            foreach (Patient p in ListePatients)
                Console.WriteLine(p.ToString());

        }

        public static void TestSelectById()
        {
            DaoPatient daoPatient = new DaoPatient();
            //On récupère le patient ayant l'id n°3
            Patient patient = daoPatient.SelectById(3);
            //On affiche l'ensemble des informations le concernant
            Console.WriteLine(patient.ToString());
        }

        public static void TestDispo()
        {
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            List<Visite> listeVisites = new List<Visite>();
            Patient patient = new Patient(1, "Dupont", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Salle salle = new Salle(1, false, patient);
            Medecin medecin = new Medecin("vLefebvre", "dvrvg327", "Lefebvre", 1, 10, salle);

            //On vérifie que la salle correspondante à bien été initialisée correctement
            Console.WriteLine(medecin.ToString());
            Console.WriteLine("Salle avant modif: " + salle.ToString());
            medecin.RendreDispo();
            //On vérifie que la salle correspondante à bien été mise à jour
            Console.WriteLine(" Salle après modif: " + salle.ToString());
            salle.Disponible = false;
            //On vérifie dans l'autre sens pour savoir si le set de disponible passe bien
            Console.WriteLine(" Salle après remodif: " + salle.ToString());
        }

        public static void TestNotifyNext()
        {
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            List<Visite> listeVisites = new List<Visite>();
            /*Création du médecin qui va devoir libérer le patient actuel, prévenir un autre patient et rendre la salle disponible
            De son côté via le notify() le patient deviendra le patient actuel la classe passera de disponible à occupée et une instance de visite
            sera ajouter au médecin, il faudra vérifier si la liste des visites a une longueur correspondant au seuil max si c'est le cas
            ajouterListes(listeVisites) devra être déclencer*/
            Patient patient = new Patient(1, "Dupont", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient2 = new Patient(2, "Dupont2", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient3 = new Patient(3, "Dupont3", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient4 = new Patient(4, "Dupont4", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient5 = new Patient(5, "Dupont5", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient6 = new Patient(6, "Dupont6", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient7 = new Patient(7, "Dupont7", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient8 = new Patient(8, "Dupont8", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient9 = new Patient(9, "Dupont9", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient10 = new Patient(10, "Dupont10", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Patient patient11 = new Patient(11, "Dupont11", "Jean", 39, "59 avenue de l`Europe", "02.45.02.12.50");

            Hopital.Instance.AjouterPatient(patient);
            Hopital.Instance.AjouterPatient(patient2);
            Hopital.Instance.AjouterPatient(patient3);
            Hopital.Instance.AjouterPatient(patient4);
            Hopital.Instance.AjouterPatient(patient5);
            Hopital.Instance.AjouterPatient(patient6);
            Hopital.Instance.AjouterPatient(patient7);
            Hopital.Instance.AjouterPatient(patient8);
            Hopital.Instance.AjouterPatient(patient9);
            Hopital.Instance.AjouterPatient(patient10);
            Hopital.Instance.AjouterPatient(patient11);
            Salle salle = new Salle(1, false, patient);
            Medecin medecin = new Medecin("vLefebvre", "dvrvg327", "Lefebvre", 1, 10, salle);
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
            medecin.notifyNext();
        }

        public static void TestAjouterVisites()
        {
            //A chaque fois que l'on atteint un certains seuil où à chaque fois qu'on le souhaite les visites sont ajouter en base
            List<Visite> listeVisites = new List<Visite>();
            //Ajout d'éléments dans une nouvelle liste
            Visite v1 = new Visite(2, "2023 - 08 - 13 08:08:00.000", "Rousseau", 2);
            Visite v2 = new Visite(3, "2023 - 07 - 31 21:51:00.000", "Lefebvre", 1);
            listeVisites.Add(v1);
            listeVisites.Add(v2);

            DaoVisite daoVisite = new DaoVisite();
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            Patient patient = new Patient(1, "Dupont", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Salle salle = new Salle(1, false, patient);
            //Intégration de la liste au médecin
            Medecin medecin = new Medecin("vLefebvre", "dvrvg327", "Lefebvre", 1, 10, salle);

            //On vérifie quelles sont les visites déjà en base 
            Console.WriteLine("Avant ajout des visites:");
            List<Visite> listeAvantAjout = daoVisite.SelectAllVisites();
            foreach (Visite v in listeAvantAjout)
                Console.WriteLine(v);

            //On ajoute les visites voulues
            medecin.AjouterVisites();

            //On vérifie que les nouvelles visites ont été intégrées à la base
            Console.WriteLine("Après ajout des visites:");
            List<Visite> listeApresAjout = daoVisite.SelectAllVisites();
            foreach (Visite v in listeApresAjout)
                Console.WriteLine(v);

            //On met en évidence les visites qui devaient être ajoutées à la base
            Console.WriteLine("Si tout bon, différence: ");
            foreach (Visite v in listeVisites)
                Console.WriteLine(v);
        }

        public static void TestAfficherFile()
        {
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            List<Visite> listeVisites = new List<Visite>();
            Patient patient = new Patient(1, "Dupont", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Salle salle = new Salle(1, false, patient);
            Medecin medecin = new Medecin("vLefebvre", "dvrvg327", "Lefebvre", 1, 10, salle);
            //List<Patient> listePatients = medecin.AfficherFile();
            //foreach (Patient p in listePatients)
            //    Console.WriteLine(p.ToString());
        }

        public static void TestToStringMedecin()
        {
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            List<Visite> listeVisites = new List<Visite>();
            Patient patient = new Patient(1, "Dupont", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Salle salle = new Salle(1, false, patient);
            Medecin medecin = new Medecin("vLefebvre", "dvrvg327", "Lefebvre", 1, 10, salle);
            Console.WriteLine(medecin.ToString());
        }

        public static void TestToStringPatient()
        {
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            List<Visite> listeVisites = new List<Visite>();
            Patient patient = new Patient(1, "Dupont", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");
            Salle salle = new Salle(1, false, patient);
            Medecin medecin = new Medecin("vLefebvre", "dvrvg327", "Lefebvre", 1, 10, salle);
            Patient patient2 = new Patient(2, "Martin", "Julien", 37, "47 avenue de l`Union", "02.51.82.21.10");
            patient2.Medecin = medecin;

            Console.WriteLine(patient.ToString());
            Console.WriteLine(patient2.ToString());
        }

        public static void TestToStringVisite()
        {
            Visite visite = new Visite(1, "2023 - 06 - 09 12:23:00.000", "Lefebvre", 1);
            Console.WriteLine(visite.ToString());
        }

        public static void TestToStringSalle()
        {
            DaoAuthentification daoAuthentification = new DaoAuthentification();
            List<Visite> listeVisites = new List<Visite>();
            Patient patient = new Patient(1, "Dupont", "Jean", 39, "53 avenue de l`Europe", "02.45.02.12.50");

            Patient patient2 = new Patient(2, "Martin", "Julien", 37, "47 avenue de l`Union", "02.51.82.21.10");
            Salle salle2 = new Salle(2, true, patient2);
            Medecin medecin = new Medecin("vLefebvre", "dvrvg327", "Lefebvre", 1, 10, salle2);
            patient2.Medecin = medecin;
            Salle salle = new Salle(1, true, patient);


            Console.WriteLine(salle.ToString());
            Console.WriteLine(salle2.ToString());
        }
    }
}
