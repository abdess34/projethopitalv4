﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class DaoPatient
    {
        private DaoAuthentification daoAuthentification=new DaoAuthentification();
        private string connectionString = @"Data Source=LAPTOP-3T9H10D3\SQLEXPRESS;Initial Catalog=hn-hopital;Integrated Security=True";

        public Patient SelectById(int idPatient)
        {
            /*"select patients.id,patients.nom, patients.prenom, patients.age, patients.adresse, patients.telephone, visites.medecin from Patients JOIN Visites ON patients.id= visites.idPatient where patients.id=" + idPatient;
            int id, string nom, string prenom, int age, string adresse, string telephone, Medecin medecin*/
            string reponse = " ID\t nom \t prenom \t age \t Adresse\t telephone \t medecin \n";
            //string connectionString = @"Data Source=LAPTOP-PP2V8KLN\SQLEXPRESS01;Initial Catalog=hn-hopital;Integrated Security=True";
            string sql = "select patients.id,patients.nom, patients.prenom, patients.age, patients.adresse, patients.telephone, visites.medecin from Patients JOIN Visites ON patients.id= visites.idPatient where patients.id=" + idPatient;

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            Patient patient=null;
            if (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                int arg1b = int.Parse(arg1);
                string arg2 = "";
                arg2 += reader.GetValue(1);
                string arg3 = "";
                arg3 += reader.GetValue(2);
                string arg4 = "";
                arg4 += reader.GetValue(3);
                int arg4b = int.Parse(arg4);
                string arg5 = "";
                arg5 += reader.GetValue(4);
                string arg6 = "";
                arg6 += reader.GetValue(5);


                patient = new Patient(arg1b, arg2, arg3, arg4b, arg5, arg6);
                reponse += reader.GetValue(0) + "\t" + reader.GetValue(1) + "\t" + reader.GetValue(2) + "\t" + reader.GetValue(3) + "\t" + reader.GetValue(4) + "\t" + reader.GetValue(5) + "\t"+ arg6 + "\t";

            }
            
            connection1.Close();

            return patient;
        }
        
        public List<Patient> SelectAllPatients() {
            //"select patients.id,patients.nom, patients.prenom, patients.age, patients.adresse, patients.telephone, visites.medecin from Patients JOIN Visites ON patients.id= visites.idPatient"

            //string connectionString = @"Data Source=LAPTOP-PP2V8KLN\SQLEXPRESS01;Initial Catalog=hn-hopital;Integrated Security=True";
            string sql = "select patients.id,patients.nom, patients.prenom, patients.age, patients.adresse, patients.telephone, visites.medecin from Patients JOIN Visites ON patients.id= visites.idPatient";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<Patient> listePatients = new List<Patient>();
            while (reader.Read())
            {
                string arg1 = "";
                arg1 += reader.GetValue(0);
                int arg1b = int.Parse(arg1);
                string arg2 = "";
                arg2 += reader.GetValue(1);
                string arg3 = "";
                arg3 += reader.GetValue(2);
                string arg4 = "";
                arg4 += reader.GetValue(3);
                int arg4b = int.Parse(arg4);
                string arg5 = "";
                arg5 += reader.GetValue(4);
                string arg6 = "";
                arg6 += reader.GetValue(5);

                Patient p = new Patient(arg1b, arg2, arg3, arg4b, arg5, arg6);
                listePatients.Add(p);
            }

            connection1.Close();
           
            return listePatients;
        }
        public bool Insert(Patient p)
        {
            SqlConnection connexion1 = new SqlConnection(connectionString);
            connexion1.Open();
            string sql = "insert into patients values(@id,@nom,@prenom,@age,@adresse,@telephone)";
            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = connection1.CreateCommand();
            command.CommandText = sql;

            command.Parameters.Add("id", SqlDbType.NVarChar).Value = p.Id;
            command.Parameters.Add("nom", SqlDbType.NVarChar).Value = p.Nom;
            command.Parameters.Add("prenom", SqlDbType.NVarChar).Value = p.Prenom;
            command.Parameters.Add("age", SqlDbType.Int).Value = p.Age;
            command.Parameters.Add("adresse", SqlDbType.NVarChar).Value = p.Adresse != null ? p.Adresse : "";
            command.Parameters.Add("telephone", SqlDbType.NVarChar).Value = p.Telephone != null ? p.Telephone : "";

            connection1.Open();
            int count = command.ExecuteNonQuery();
            connection1.Close();
            return count > 0 ? true : false;
        }
        public bool Update(Patient p)
        {
            SqlConnection connexion1 = new SqlConnection(connectionString);
            connexion1.Open();
            string sql = "update  patients set adresse =@adresse,telephone =@telephone where id = @id";
            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = connection1.CreateCommand();
            command.CommandText = sql;

            command.Parameters.Add("id", SqlDbType.Int).Value = p.Id;
            command.Parameters.Add("adresse", SqlDbType.NVarChar).Value = p.Adresse;
            command.Parameters.Add("telephone", SqlDbType.NVarChar).Value = p.Telephone;


            connection1.Open();
            int count = command.ExecuteNonQuery();
            connection1.Close();
            return count > 0 ? true : false;
        }
    }   
}
