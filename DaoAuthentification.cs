﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class DaoAuthentification
    {
        private string connectionString = @"Data Source=LAPTOP-3T9H10D3\SQLEXPRESS;Initial Catalog=hn-hopital;Integrated Security=True";

        /// <summary>
        /// permet de tester si un utilisteur existe en base ou non
        /// </summary>
        /// <param name="login">adresse mail </param>
        /// <param name="psw"> mot de passe</param>
        /// <returns>retourne un objet de type static Authentification pouvant etre soit 
        /// une authentification pour un medecin ou pour une secretaire  </returns>
        public Authentification SelectUserByLoginPsw(string login, string psw)
        {
            Authentification auth = null;
            //string connectionString = @"Data Source=LAPTOP-3T9H10D3\SQLEXPRESS;Initial Catalog=hn;Integrated Security=True";
            string sql = "select * from authentification where login= @login and password =@psw";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = connection1.CreateCommand();
            command.CommandText = sql;

            command.Parameters.Add("login", SqlDbType.NVarChar).Value = login;
            command.Parameters.Add("psw", SqlDbType.NVarChar).Value = psw;
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();
            //Console.WriteLine("nb trouver: " + reader.GetValue(0));
            if (reader.Read())
            {
                if (reader.GetInt32(3) == 0)
                {
                    auth = new Secretaire(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));

                }
                else if (reader.GetInt32(3) == -1)
                {
                    auth = new Admin(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));

                }
                else
                    auth = new Medecin(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));
            }

            connection1.Close();

            return auth;
        }

        public List<Medecin> SelectAllMedecin()
        {

            List<Medecin> list = new List<Medecin>();
            //string connectionString = @"Data Source=LAPTOP-3T9H10D3\SQLEXPRESS;Initial Catalog=hn;Integrated Security=True";
            string sql = "select * from authentification where metier <>0";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                list.Add(new Medecin(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3)));              
            }

            connection1.Close();

            return list;

        }
        public Secretaire SelectSecretaire()
        {

            Secretaire secretaire = null;
            //string connectionString = @"Data Source=LAPTOP-3T9H10D3\SQLEXPRESS;Initial Catalog=hn;Integrated Security=True";
            string sql = "select * from authentification where metier = 0";

            SqlConnection connection1 = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand(sql, connection1);
            connection1.Open();
            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                secretaire = new Secretaire(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(3));
            }

            connection1.Close();

            return secretaire;

        }
    }
}
