﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    public class Medicament
    {
        private int id;
        private string nom;
        private int prix;
        private int quantite;

        public Medicament(int id, string nom, int prix, int qte)
        {
            this.id = id;
            this.nom = nom;
            this.prix = prix;
            this.quantite = qte;
        }
        public int Id
        {
            get { return id; }
        }
        public double Quantite
        {
            get { return quantite; }

        }
        public string Nom
        {
            get { return nom; }
        }
        public int Prix
        {
            get { return prix; }
        }

        public bool DebiterStock(int nb)
        {
            if (nb > 0 && (this.quantite - nb) >= 0)
            {
                quantite -= nb;
                return true;
            }
            return false;
        }
        public override string ToString()
        {
            return "MEDICAMENT: " + id + " NOM : " + nom + " PRIX : " + prix + " STOCK : " + quantite;
        }
    }
}
