﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    [Serializable]
    class Patient
    {
        private int id;
        private string nom;
        private string prenom;
        private int age;
        private string adresse;
        private string telephone;
        private Medecin medecin;
        private DateTime debut,fin;



        public Patient(int id, string nom, string prenom, int age, string adresse, string telephone)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.age = age;
            this.adresse = adresse;
            this.telephone = telephone;
            this.medecin = null;
        }
        public Patient(int id, string nom, string prenom, int age)
        {
            this.id = id;
            this.nom = nom;
            this.prenom = prenom;
            this.age = age;
            this.adresse = null;
            this.telephone = null;
            this.medecin = null;
        }
        public int Id
        {
            get { return id; }
        }
        public int Age
        {
            get { return age; }
        }
        public string Nom
        {
            get { return nom; }
        }
        public string Prenom
        {
            get { return prenom; }
        }
        public string Adresse
        {
            get { return adresse; }
            set { adresse = value; }
        }
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }
        public DateTime Debut
        {
            get { return debut; }
            set {  debut= value; }
        }
        public DateTime Fin
        {
            get { return fin; }
            set { fin = value; }
        }
        public Medecin Medecin
        {
            get
            {
                return medecin;
            }
            set
            {
                medecin=value;
            }
        }

        

        public void notify()
        {
            DateTime f = DateTime.Now;
            this.Fin = f;
            Visite visite = new Visite(Id, f.ToString(),  medecin.Nom, medecin.Salle.NumSalle);
            visite.Attente = (this.Fin - this.Debut);
            medecin.Attache(visite);
            Hopital.Instance.FileDattente.Dequeue();
        }

        public override string ToString()
        {

            string reponse = "Patient:Id: " + id + " Nom: " + nom + " Prénom: " + prenom + " Age: " + age + " Adresse: " + adresse + " Téléphone: " + telephone;
            if (medecin != null)
                reponse += " medecin" + medecin.ToString();
            else
                reponse += " Pas de médecin affecté";

            return (reponse);
        }

    }

    //id,nom ,prenom ,age ces champs sont obligatoires, option : téléphone / adresse
    /*: numéro de visite(auto),Id du patient(numéro de sécu-unique), nom du médecin , le cout
    de la visite(tarif fixe = 23 e ) ainsi que la date de la visite(une chaine), le numero de la salle*/
}
