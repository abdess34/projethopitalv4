﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class Program
    {
        
        
        private static Queue<Patient> queuetempo = new Queue<Patient>();
        public static void AfficheEntete()
        {
            Console.WriteLine("********************************************************************");
            Console.WriteLine("**************** Bienvenue au service Hospitalier ******************");
            Console.WriteLine("********************************************************************");
        }
        public static void AfficheChoixInterface()
        {
            Console.WriteLine("[1] -> Interface Patient");
            Console.WriteLine("[2] -> Interface Personnels de l'hopital");
            Console.WriteLine("----------------------------------------");
        }
        public static void AfficheInterfaceSecretaire()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("****  OPERATIONS DE LA SECRETAIRE  ****");
            Console.WriteLine("[1] -> Ajouter un patient a la file d’attente ");
            Console.WriteLine("[2] -> Afficher la file d’attente");
            Console.WriteLine("[3] -> Afficher le prochain patient de la file");
            Console.WriteLine("[4] -> Modifier/completer la fiche d'un patient");
            Console.WriteLine("[5] -> Afficher historique des visite d'un patient");
            Console.WriteLine("[6] -> Charger la liste d'attente ");
            Console.WriteLine("[7] -> Sauvegarder la liste d'attente ( sérialiser) ");
            Console.WriteLine("[8] -> Afficher la liste des visites dun patient triée par date+heure asc");
            Console.WriteLine("[9] -> Afficher la liste des visites dun patient triées par medecin(depuis c#) ");
            Console.WriteLine("[10] -> Afficher le nombre de visites pour un patient donné depuis le debut de son activité ");
            Console.WriteLine("[11] -> Afficher le nombre de visites pour un patient donné entre une date a et une date b ");
            Console.WriteLine("[12] -> afficher la liste des medicaments (ordonnance) d un patient selon un id+  date et heure ");
            Console.WriteLine("[13] -> Retour au menu principal");
            Console.WriteLine("----------------------------------------");
        }
        public static void AfficheInterfaceMedecin()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("****      OPERATIONS DU MEDECIN     ****");
            Console.WriteLine("[1] -> Rendre la salle disponible ");
            Console.WriteLine("[2] -> Afficher la file d’attente");
            Console.WriteLine("[3] -> Afficher les visites en base");
            Console.WriteLine("[4] -> Afficher les visites actuelles");
            Console.WriteLine("[5] -> Sauvergarder les visites en base");
            Console.WriteLine("[6] -> Retour au menu principal");
            Console.WriteLine("----------------------------------------");
        }
        public static void AfficheInterfaceAdmin()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("****      OPERATIONS De L'ADMIN     ****");
            Console.WriteLine("[1] -> Ajouter un nouveau medicament ");
            Console.WriteLine("[2] -> Supprimer un medicament ");
            Console.WriteLine("[3] -> Ajouter de la quantité à un medicament existant");
            Console.WriteLine("[4] -> Temps d'attente d'un patient/date");
            Console.WriteLine("[5] -> Nombre de visites pour une salle et un medecin (tries)");
            Console.WriteLine("[6] -> Le prix moyen des visites  pour un medecin depuis le debut a aujourdhui");
            Console.WriteLine("[7] -> Le prix moyen des visites  pour un medecin entre une date min et max");
            Console.WriteLine("[8] -> Retour au menu principal");
            Console.WriteLine("----------------------------------------");
        }
        public static int RecuperChoixUtilisateur()
        {
            Console.WriteLine("Entrer votre choix : ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public static void manage()
        {
            AfficheChoixInterface();
            int mode = RecuperChoixUtilisateur();

            switch (mode)
            {
                case 1: // Patient
                    AffichagePatient();
                    manage();
                    break;
                case 2: // Interne Hopital
                    Authentification auth = ConnexionSecretaireMedecin();

                    if (auth is Secretaire)
                    {
                        auth = (Secretaire)auth;
                        Console.WriteLine("----------------------------------------");
                        Console.WriteLine("             Interface Secretaire          ");
                        Console.WriteLine("nom: " + auth.Nom + " metier: " + auth.Metier);
                        Secretaire reference = null;
                        //pointer la reference vers l'obj secretaire cree en interne de Hopital
                        if (Hopital.Instance.Secretaire.Login == auth.Login &&
                           Hopital.Instance.Secretaire.Password == auth.Password)
                        {
                            reference = Hopital.Instance.Secretaire;
                        }
                        else
                        {
                            Console.WriteLine("erreur connexion ..."); return;
                        }

                        int operation;
                        do
                        {
                            AfficheInterfaceSecretaire();
                            operation = RecuperChoixUtilisateur();
                            switch (operation)
                            {
                                case 1: // Ajouter un patient a la file d’attente
                                    Console.WriteLine("----------------------------------------");
                                    DaoPatient dao = new DaoPatient();
                                    if (queuetempo.Count == 0)
                                    {
                                        Console.WriteLine("aucun patient à ajouter");
                                        break;
                                    }
                                    Patient tete = queuetempo.Dequeue();
                                    Patient recupBDD = dao.SelectById(tete.Id);
                                    if (recupBDD == null)
                                    {
                                        Console.WriteLine("Nouveau Patient, ajout a la base");
                                        dao.Insert(tete);
                                        Console.WriteLine("Ajout à la file d'attente:");
                                        Console.WriteLine(tete);
                                        reference.AjouterPatientFileAttente(tete);
                                        reference.SauvergarderDansFichierPatient(tete);
                                        Console.WriteLine("Sauvergarde du patient dans un fichier txt");
                                    }
                                    else
                                    {
                                        if (reference.AjouterPatientFileAttente(recupBDD))
                                        {
                                            Console.WriteLine("Patient déja dans la file.");
                                        }
                                        else
                                        {
                                            Console.WriteLine("Patient connu de l'hopital:");
                                            Console.WriteLine("Ajout à la file d'attente:");
                                            Console.WriteLine(recupBDD);
                                            //reference.AjouterPatientFileAttente(recupBDD);
                                            reference.SauvergarderDansFichierPatient(recupBDD);
                                            Console.WriteLine("Sauvergarde du patient dans un fichier txt");
                                        }

                                    }
                                    Console.WriteLine("----------------------------------------");

                                    break;
                                case 2: //afficher la file d'attente
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine(Hopital.Instance.ToString());
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 3: // aficher le prochain patient dans la file
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("             Prochain patient          ");
                                    Console.WriteLine(Hopital.Instance.ProchainPatientDansFile());
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 4: // modifier/completer la fiche d'un patient
                                    CompleterfichePatient();
                                    break;
                                case 5: // historique des visites d'un patient
                                    HistoriqueVisite();

                                    break;
                                case 6: // chargement de la file dattente
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("           Chargement de la file        ");
                                    reference.ChargerFileAttente();
                                    Console.WriteLine("----------------------------------------");

                                    break;
                                case 7: // historique des visites d'un patient
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("     Sauvergarder la file et quitter    ");
                                    reference.SauvergarderListAttente();
                                    Console.WriteLine("----------------------------------------");
                                    manage();
                                    break;

                                case 8:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("  Liste des visites dun patient triée par date+heure asc (tri depuis la base)  ");
                                    Console.WriteLine("saisir un id Patient:");
                                    int id = Convert.ToInt32(Console.ReadLine());
                                    foreach(Visite v in  new DaoVisite().selectAllVisitesByPatient(id))
                                    {
                                        Console.WriteLine(v);
                                    }
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 9:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("  Listes des visites tries par medecin  ");
                                    Console.WriteLine("id patient: ");
                                    int choix =RecuperChoixUtilisateur();
                                    Visite[] lesVisites = new DaoVisite().SelectByPatient(choix).ToArray();
                                    Array.Sort(lesVisites, new VisiteSortMedecin());
                                    foreach (Visite v in lesVisites)
                                        Console.WriteLine(v);
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 10:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("  Nombre de visites pour un patient donné depuis le debut de son activité  ");
                                    Console.WriteLine("id patient: ");
                                    choix = RecuperChoixUtilisateur();
                                    int val = new DaoVisite().GetNbVisitesByPatient(choix);
                                    Console.WriteLine("le patient {0} a eu {1} visites", choix, val);
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 11:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Nombre de visites pour un patient donné entre une date a et une date b  ");
                                    Console.WriteLine("id patient: ");
                                    choix = RecuperChoixUtilisateur();
                                    Console.WriteLine("Saisir une date min:");
                                    string min = Console.ReadLine();
                                    Console.WriteLine("Saisir une date max:");
                                    string max = Console.ReadLine();

                                    val = new DaoVisite().GetNbVisitesByPatientAndDate(choix, DateTime.Parse(min), DateTime.Parse(max));
                                    Console.WriteLine("le patient {0} a eu {1} visites entre le {2} et le {3}", choix, val,min,max);
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 12:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("afficher la liste des medicaments (ordonnance) d un patient selon un id+  date et heure ");
                                    afficheListeMedicament();
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 13: // Sauvergarder les arrivant dans un .txt
                                    Console.WriteLine("Fermeture interface Secretaire");
                                    Console.WriteLine("----------------------------------------");
                                    manage();
                                    break;
                            }
                        } while (operation != 13);

                    }
                    else if (auth is Medecin)
                    {
                        auth = (Medecin)auth;
                        Console.WriteLine("----------------------------------------");
                        Console.WriteLine("             Interface Medecin          ");
                        Console.WriteLine("nom: " + auth.Nom + " metier: " + auth.Metier);
                        Medecin reference = null;
                        //pointer la reference vers l'obj medecin cree en interne de Hopital
                        foreach (Medecin m in Hopital.Instance.ListeMedecins)
                        {
                            if (m.Login == auth.Login && m.Password == auth.Password) reference = m;

                        }
                        if (reference == null)
                        {
                            Console.WriteLine("erreur connexion ..."); return;
                        }
                        int operation;
                        do
                        {
                            AfficheInterfaceMedecin();
                            operation = RecuperChoixUtilisateur();
                            switch (operation)
                            {
                                case 1: //Rendre la salle disponible
                                    reference.RendreDispo();
                                    Patient p = reference.notifyNextV2();
                                    if (p != null)
                                    {
                                        Console.WriteLine("patient actuelle en salle:");
                                        Console.WriteLine(p);
                                    }
                                    else
                                    {
                                        Console.WriteLine("plus aucun patient!");
                                    }
                                    AffichageOrdonnance(reference.GetLastVisiste());
                                    break;
                                case 2: // Afficher la file d’attente
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine(Hopital.Instance.ToString());
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 3: // Afficher les visites
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("             Mes Visites en base          ");
                                    foreach (Visite v in new DaoVisite().SelectByMedecin(reference.Nom))
                                    {
                                        Console.WriteLine(v);
                                    }
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 4: // Sauvergarder les visites actu
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Mes visites actuelles:");
                                    foreach (Visite v in reference.ListeVisites)
                                    {
                                        Console.WriteLine(v);
                                    }
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 5: // Sauvergarder les visites en base
                                    Console.WriteLine("Mes visites en base:");
                                    foreach (Visite v in reference.ListeVisites)
                                    {
                                        Console.WriteLine(v);
                                    }
                                    reference.AjouterVisites();
                                    break;
                                case 6: // Retour au menu principal
                                    Console.WriteLine("Fermeture interface Medecin");
                                    Console.WriteLine("----------------------------------------");
                                    manage();
                                    break;
                            }
                        } while (operation != 6);

                    }
                    else // CAS: ADMIN
                    {
                        auth = (Admin)auth;
                        Console.WriteLine("----------------------------------------");
                        Console.WriteLine("             Interface Admin          ");
                        Console.WriteLine("nom: " + auth.Nom + " metier: " + auth.Metier);
                        int operation;
                        do
                        {
                            AfficheInterfaceAdmin();
                            operation = RecuperChoixUtilisateur();
                            switch (operation)
                            {
                                case 1:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Ajouter un nouveau medicament:");
                                    Console.WriteLine("----------------------------------------");
                                    break;

                                case 2:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Supprimer un  medicament:");
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 3:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Ajouter de la quantité à un medicament existant:");
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 4:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Temps d'attente d'un patient:");
                                    Console.WriteLine("saisir id patient: ");
                                    int idp = Convert.ToInt32(Console.ReadLine());
                                    Console.WriteLine("saisir une date : ");
                                    string date = Console.ReadLine();//"2023-11-01 20:21:18.000"
                                    string temps =new DaoVisite().SelectAttenteByIdDate(idp, DateTime.Parse(date));

                                    Console.WriteLine("Le patient {0} a attendu {1} ", idp, temps);
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 5:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Nombre de visites pour une salle et un medecin (tries):");
                                    foreach(string s in new DaoVisite().SelectNbVisiteSalleMedecin())
                                    {
                                        Console.WriteLine(s);
                                    }
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 6: 
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Le prix moyen des visites  pour un medecin depuis le debut a aujourdhui:");
                                    Console.WriteLine("Saisir un nom de medecin:");
                                    string nom = Console.ReadLine();
                                    double val = new DaoVisite().GetTarifMoyByMedecin(nom);
                                    Console.WriteLine("Prix moyen des visistes: " + val);
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 7:
                                    Console.WriteLine("----------------------------------------");
                                    Console.WriteLine("Prix moyen des visites  pour un medecin entre une date min et max:");
                                    Console.WriteLine("Saisir un nom de medecin:");
                                    nom = Console.ReadLine();
                                    Console.WriteLine("Saisir une date min:");
                                    string min = Console.ReadLine();
                                    Console.WriteLine("Saisir une date max:");
                                    string max = Console.ReadLine();

                                    val = new DaoVisite().GetTarifMoyByMedecinAndDate(nom, DateTime.Parse(min), DateTime.Parse(max));
                                    Console.WriteLine("Prix moyen des visistes entre le {0} et le {1} est de {2}: ",min,max, val);
                                    Console.WriteLine("----------------------------------------");
                                    break;
                                case 8:
                                    Console.WriteLine("Fermeture interface Admin");
                                    Console.WriteLine("----------------------------------------");
                                    manage();
                                    break;
                            }
                        } while (operation != 8);
                    }
                    break;
            }
        }
        static void HistoriqueVisite()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("     HISTORIQUE DES VISITES PATIENT     ");
            Console.WriteLine("saisir id Patient: ");
            int numSecu = Convert.ToInt32(Console.ReadLine());
            List<Visite> visites = new DaoVisite().SelectByPatient(numSecu);
            if (visites.Count != 0)
            {
                foreach (Visite v in visites) Console.WriteLine(v);
            }
            else
            {
                Console.WriteLine("Aucun historique pour le patient: " + numSecu);
            }
            Console.WriteLine("----------------------------------------");
        }
        static void CompleterfichePatient()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("     COMPLETER/MODIFIER FICHE PATIENT   ");
            Console.WriteLine("saisir id Patient: ");
            int numSecu = Convert.ToInt32(Console.ReadLine());
            Patient p = new DaoPatient().SelectById(numSecu);
            while (p == null)
            {

                Console.WriteLine("saisir id Patient: ");
                numSecu = Convert.ToInt32(Console.ReadLine());
                //p = getPatientById(numSecu);
                p = new DaoPatient().SelectById(numSecu);
            }
            Console.WriteLine("Patient trouvé: ");
            Console.WriteLine("Saisir adresse: ");
            string adr = Console.ReadLine();
            Console.WriteLine("Saisir telephone: ");
            string telephone = Console.ReadLine();
            p.Telephone = telephone; p.Adresse = adr;
            if (new DaoPatient().Update(p)) Console.WriteLine("Modification OK");
            else Console.WriteLine("Modification KO");
            Console.WriteLine("----------------------------------------");
        }
        public static Patient getPatientById(int id)
        {
            Patient patient = null;
            foreach (Patient p in Hopital.Instance.FileDattente)
            {
                if (p.Id == id)
                {
                    return p;
                }
            }
            return patient;
        }
        static Authentification ConnexionSecretaireMedecin()
        {
            Authentification auth;
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("****   AUTHENTIFICATION A LA BASE   ****");
            Console.WriteLine("----------------------------------------");
            do
            {
                Console.WriteLine("Veuillez entrer votre nom d'utilisateur:");
                string login = Console.ReadLine();
                Console.WriteLine("Veuillez entrer votre mot de passe:");
                string password = Console.ReadLine();
                auth = Authentificate(login, password);
                if (auth == null)
                {
                    Console.WriteLine("Nom d'utilisateur ou mot de passe incorrect! Réessayez !");

                }
            } while (auth == null);
            return auth;
        }
        static Authentification Authentificate(string login, string password)
        {
            return new DaoAuthentification().SelectUserByLoginPsw(login, password);
        }
        static void AffichagePatient()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("             Interface Patient          ");
            Console.WriteLine("saisir votre Num Securite sociale: ");
            int numSecu = Convert.ToInt32(Console.ReadLine());
            Patient patient = new DaoPatient().SelectById(numSecu);
            if (patient == null)
            {
                Console.WriteLine("Vous etes nouveau dans l'hopital: ");
                Console.WriteLine("saisir votre Nom: ");
                string nom = Console.ReadLine();
                Console.WriteLine("saisir votre Prenom: ");
                string prenom = Console.ReadLine();
                Console.WriteLine("saisir votre Age: ");
                int age = Convert.ToInt32(Console.ReadLine());
                patient = new Patient(numSecu, nom, prenom, age);

            }
            else
            {
                Console.WriteLine("Vous etes connu de notre service, une receptionniste va s'occuper de vous.");
            }
            queuetempo.Enqueue(patient);
            Console.WriteLine("Fermeture interface Patient");
            Console.WriteLine("----------------------------------------");
        }



        public static void AffichageOrdonnance(Visite v)
        {
            
            string reponse;
            do
            {
                Console.WriteLine("Voulez vous une ordonnance(y/n): ");
                reponse = Console.ReadLine();
            } while (reponse != "y" && reponse != "n");
            if (reponse == "y")
            {
                ChoixMedicaments(v);
            }
        }
        public static void ChoixMedicaments(Visite v)
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("             Creation Ordonnace         ");
            Console.WriteLine("          Liste des Medicaments         ");
            List<Medicament> listeM = Hopital.Instance.ListeMedicaments;
            foreach (Medicament m in listeM)
            {
                Console.WriteLine("----------------------------------------------------------");
                Console.WriteLine(m);
                Console.WriteLine("----------------------------------------------------------");
            }
            int rep = -1;
            int cmp = 0;
            Ordonnance ordo = new Ordonnance(v.IdPatient);
            while (cmp != listeM.Count)
            {
                string reponse;
                do
                {
                    Console.WriteLine("Voulez-vous le medicament(y/n):" + listeM[cmp]);
                    reponse = Console.ReadLine();
                } while (reponse != "y" && reponse != "n");
                if (reponse == "y")
                {
                    Console.WriteLine("Saisir la quantite:");
                    int qte = Convert.ToInt32(Console.ReadLine());
                    while (!listeM[cmp].DebiterStock(qte))
                    {
                        Console.WriteLine("Il n'en reste pas assez ...");
                        Console.WriteLine("Saisir la quantite:");
                        qte = Convert.ToInt32(Console.ReadLine());
                    }
                    Ligne l = new Ligne(listeM[cmp], qte);
                    ordo.AjouterLigne(l);

                }
                cmp++;
            }
            foreach (Medicament m in listeM) new DAOMedicament().Update(m);
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("          ORDONNANCE DU MEDECIN         ");
            Console.WriteLine(ordo);
            AjouterOrdonnanceVisite(v,ordo);
            Console.WriteLine("----------------------------------------");
        }
        public static void AjouterOrdonnanceVisite(Visite v,Ordonnance o)
        {
            string colonneOrdonnance = "";
            foreach (Ligne l in o.MesLignes)
            {
                colonneOrdonnance += l.Qte + "-" + l.Medicament.Id + "/";
            }
            
            v.Ordo = colonneOrdonnance;
            v.Tarif += o.PrixT;
            //new DaoVisite().Update(v);
        }
        static void Main(string[] args)
        {

            Hopital h = Hopital.Instance;
            //Queue<Patient> q = new Queue<Patient>();
            //q.Enqueue(new Patient(1, "fred", "fred", 10));
            //q.Enqueue(new Patient(2, "Eliot", "Jacky", 20));
            //q.Enqueue(new Patient(3, "Emile", "Louis", 20));
            //h.AffecterFileAttente(q);
            //h.Secretaire.SauvergarderListAttente();
            //h.Secretaire.ChargerFileAttente();
            //Console.WriteLine(h.ToString());

            AfficheEntete();
            manage();
            //string date = "2023-11-02 16:35:44.000";
            //Console.WriteLine(new DaoVisite().SelectAttenteByIdDate(1, DateTime.Parse(date)));
            //Medicament m = new DAOMedicament().SelectById(2);
            //Console.WriteLine(m);
            //afficheListeMedicament();
            //Console.WriteLine(Hopital.Instance.ToString());
        }

        static void afficheListeMedicament()
        {
            Console.WriteLine("saisir id patient: ");
            int idp = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("saisir une date : ");
            string date = Console.ReadLine();//"2023-11-01 20:21:18.000"
            
            Visite v = new DaoVisite().SelectVisiteByPatientDate(idp, DateTime.Parse(date));
            Patient p = new DaoPatient().SelectById(v.IdPatient);
            
            Console.WriteLine(gestionOrdo(p.Nom, p.Prenom, v.Ordo));
        }
        static Ordonnance gestionOrdo(string nom,string prenom,string ordo)
        {
            string[] qteMed = ordo.Split('/');
            Ordonnance o = new Ordonnance(nom, prenom);
            
            for(int i=0; i < qteMed.Length - 1; i++)
            {
                string[] qteMed2 = qteMed[i].Split('-');
                int qte = Convert.ToInt32(qteMed2[0]);
                int idMed = Convert.ToInt32(qteMed2[1]);
                Medicament m = new DAOMedicament().SelectById(idMed);
                Ligne l = new Ligne(m, qte);
                o.AjouterLigne(l);
            }
            return o;
        }

    }





}

