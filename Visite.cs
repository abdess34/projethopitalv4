﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetHopital_HN
{
    class Visite
    {

        private int id;
        private int idPatient;
        private String date;
        private String nomMedecin;  
        private int numSalle;
        private int tarif;
        private string ordo = "";
        private TimeSpan attente;
        public int IdPatient {
            get
            {
                return idPatient;
            }
        }
        public TimeSpan Attente
        {
            get { return attente; }
            set {  attente = value; }
        }
        public int Id
        {
            get
            {
                return id;
            }
        }
        public String Date
        {
            get
            {
                return date;
            }
        }

        public String NomMedecin
        {
            get
            {
                return nomMedecin;
            }
        }
        public String Ordo
        {
            get
            {
                return ordo;
            }
            set { ordo = value; }
        }

        public int NumSalle
        {
            get
            {
                return numSalle;
            }
        }

        public int Tarif
        {
            get
            {
                return tarif;
            }
            set { tarif = value; }
        }

        //Lorsque l'on extrait une visite de la base on récupère les informations
        public Visite(int id, int idPatient, string date, string nomMedecin, int numSalle)
        {
            this.id = id;
            this.idPatient = idPatient;
            this.date = date;
            this.nomMedecin = nomMedecin;
            this.numSalle = numSalle;
            this.tarif = 23;        //Tarif fixe à 23 euros !!! À vérifier doit on garder le tarif dans les paramètres du constructeur de visite!!!
        }

        //Lorsque l'on inscrit les informations dans la base on doit prendre en compte le fait que l'id est auto-incrémenté et donc ne pas la saisir
        public Visite( int idPatient, string date, string nomMedecin, int numSalle)
        {         
            this.idPatient = idPatient;
            this.date = date;
            this.nomMedecin = nomMedecin;
            this.numSalle = numSalle;
            this.tarif = 23;        //Tarif fixe à 23 euros !!! À vérifier doit on garder le tarif dans les paramètres du constructeur de visite!!!
        }
        public Visite(int id, int idPatient, string date, string nomMedecin, int numSalle, int tarif)
        {
            this.id = id;
            this.idPatient = idPatient;
            this.date = date;
            this.nomMedecin = nomMedecin;
            this.numSalle = numSalle;
            this.tarif = tarif;        //Tarif fixe à 23 euros !!! À vérifier doit on garder le tarif dans les paramètres du constructeur de visite!!!
        }
        public Visite(int id, int idPatient, string date, string nomMedecin, int numSalle, int tarif,string ordo)
        {
            this.ordo = ordo;
            this.id = id;
            this.idPatient = idPatient;
            this.date = date;
            this.nomMedecin = nomMedecin;
            this.numSalle = numSalle;
            this.tarif = tarif;        //Tarif fixe à 23 euros !!! À vérifier doit on garder le tarif dans les paramètres du constructeur de visite!!!
        }
        public override string ToString()
        {

            return "Visite:Id: " +id+" Numéro du patient: "+idPatient+" Nom du médecin: "+nomMedecin+" Tarif: "+tarif+" Date: "+date+" Numéro de salle: "+numSalle + " Ordo : " + ordo;
        }
    }
}
